<?php
/**
 * Définit l'administration du plugin Orgchart.js
 *
 * Installation et désinstallation du plugin
 *
 * @plugin     orgchartjs
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Orgchartjs\Administrations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Création et mise à jour du plugin Orgchart.js
 *
 * @param  string $nom_meta_base_version version du schéma de données du plugin installé
 * @param  string $version_cible Version déclarée dans paquet.xml
 * @return void
**/

function orgchartjs_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	$maj['create'] = array(
		array('orgchartjs_create'),
	);
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Déclare la configuration d'OrgChartjs
 *
 **/
function orgchartjs_create() {
	include_spip('inc/config');
	ecrire_config('orgchartjs/espace_prive', 'on');
	ecrire_config('orgchartjs/espace_public', 'on');
}
/**
 * Faire le ménage lors de la désinstallation du plugin OrgChart.js
 *
 * @param  string $nom_meta_base_version version du schéma de données du plugin installé
 * @return void
**/

function orgchartjs_vider_tables($nom_meta_base_version) {
	effacer_meta('orgchartjs');
	effacer_meta($nom_meta_base_version);
}

