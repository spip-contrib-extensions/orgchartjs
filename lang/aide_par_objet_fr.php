<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

'configurer_orgchartjs_texte'	=> 	'Dans l’espace privé, lors de la visualisation du contenu de vos objets éditoriaux, vous bénéficiez d’une présentation graphique de leur ascendence. Savez-vous qu’il vous est possible de personnaliser cette présentation ? Il vous faut créer un squelette spécifique dans prive/squelettes/inclure/vue_orgchartjs_OBJET',
'configurer_orgchartjs_titre'	=> 	'Customisation'

);
