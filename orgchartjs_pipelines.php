<?php
/**
 * Utilisations de pipelines par Orgchartjs
 *
 * @plugin     orgchartjs
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Orgchartjs\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Inserer les CSS de orgchart
 *
 * @pipeline insert_head_css
 * @link https://programmer.spip.net/insert_head_css-561 description du pipeline
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function orgchartjs_insert_head_css($flux){
	include_spip('inc/config');
	if (lire_config('orgchartjs/publique') != 'non') {
		// la feuille de style du plugin OrgChart.js pour jquery
		$flux .= '<link rel="stylesheet" type="text/css" href="'.find_in_path("orgchart-master/dist/css/jquery.orgchart.css").'" />';
		// la feuille de style du plugin OrgChart.js pour SPIP
		$flux .= '<link rel="stylesheet" type="text/css" href="'.find_in_path("css/spip_orgchartjs.css").'" />';
  	}
	return $flux;
}

/**
 * Inserer le javascript de orgchartjs
 *
 * @pipeline insert_head
 * @link https://programmer.spip.net/insert_head description du pipeline
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function orgchartjs_insert_head($flux){
	include_spip('inc/config');
	if (lire_config('orgchartjs/publique') != 'non') {
		$flux .= "<script type='text/javascript' src='".find_in_path('orgchart-master/dist/js/jquery.orgchart.js')."'></script>"
			. "<script type='text/javascript' src='".find_in_path('orgchart-master/dist/js/jquery.orgchart.min.js')."'></script>";
	}
	return $flux;
}

/**
 * Activer orgchartjs dans la partie privée
 *
 * @pipeline header_prive
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function orgchartjs_header_prive($flux){
	include_spip('inc/config');
	if (lire_config('orgchartjs/privee') != 'non') {
		$flux .= orgchartjs_insert_head('');
		$flux .= orgchartjs_insert_head_css('');
	}
	return $flux;
}


