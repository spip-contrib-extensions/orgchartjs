<?php
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 * @link : https://contrib.spip.net/Formulaire-de-configuration-avec-le-plugin-Saisies
 *
 * @return $saisies Tableau décrivant les saisies à afficher dans le formulaire de configuration
 **/
function formulaires_configurer_orgchartjs_saisies_dist(){

	$saisies = [
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'espace_prive',
				'label_case' => '<:orgchartjs:oui:>',
				'explication' => '<:orgchartjs:config_explication_prive:>',
				'conteneur_class' => 'pleine_largeur'
			 ]
		],
		[
			'saisie' => 'case',
			'options' => [
				'nom' => 'espace_public',
				'label_case' => '<:orgchartjs:oui:>',
				'explication' => '<:orgchartjs:config_explication_public:>',
				'conteneur_class' => 'pleine_largeur'
			]
		]
	];

	return $saisies;

}
